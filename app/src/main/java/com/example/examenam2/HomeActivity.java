package com.example.examenam2;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class HomeActivity extends AppCompatActivity {
  Button registerButton;
  SwipeRefreshLayout swipeRefreshLayout;
  RecyclerView recyclerView;
  RVAdapter adapter;
  DAOIndustry dao;
  boolean isLoading = false;
  String key = null;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_home);

    setTitle("Inicio");

    // Buttons
    registerButton = (Button) findViewById(R.id.registerbutton);

    registerButton.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        showRegister();
      }
    });

    // Item Click Listener
    RVAdapter.ItemClickListener itemClickListener = new RVAdapter.ItemClickListener() {
      @Override
      public void onItemClick(Industry industry) {
        showDetail(industry);
      }
    };

    // Swipper Layout
    swipeRefreshLayout = findViewById(R.id.swiper);
    recyclerView = findViewById(R.id.rv);
    recyclerView.setHasFixedSize(true);
    LinearLayoutManager manager = new LinearLayoutManager(this);
    recyclerView.setLayoutManager(manager);
    adapter = new RVAdapter(this, itemClickListener);
    recyclerView.setAdapter(adapter);

    // Set data
    dao = new DAOIndustry();
    loadData();

    // List listeners
    recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
      @Override
      public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
        LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
        int totalItem = linearLayoutManager.getItemCount();
        int lastVisible = linearLayoutManager.findLastVisibleItemPosition();

        if (totalItem < lastVisible + 3) {
          if (!isLoading) {
            isLoading = true;
            loadData();
          }
        }
      }
    });
  }

  private void showRegister() {
    Intent registerIntent = new Intent(this, RegisterActivity.class);
    startActivity(registerIntent);
  }

  private void showDetail(Industry industry) {
    Intent detailIntent = new Intent(this, DetailActivity.class);
    detailIntent.putExtra("object_key", industry.getKey());
    startActivity(detailIntent);
  }

  private void loadData() {
    swipeRefreshLayout.setRefreshing(true);

    dao.get(key).addValueEventListener(new ValueEventListener() {
      @Override
      public void onDataChange(@NonNull DataSnapshot snapshot) {
        ArrayList<Industry> industries = new ArrayList<>();

        for (DataSnapshot data: snapshot.getChildren()) {
          Industry indus = data.getValue(Industry.class);
          indus.setKey(data.getKey());
          industries.add(indus);
          key = data.getKey();
        }

        adapter.setItems(industries);
        adapter.notifyDataSetChanged();
        isLoading = false;
        swipeRefreshLayout.setRefreshing(false);
      }

      @Override
      public void onCancelled(@NonNull DatabaseError error) {
        swipeRefreshLayout.setRefreshing(false);
      }
    });
  }
}