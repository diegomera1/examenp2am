package com.example.examenam2;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;

public class RegisterActivity extends AppCompatActivity {
  EditText industriaNombreE;
  EditText representanteE;
  EditText emailE;
  EditText areaIndustriaE;
  Button saveIndustryButton;
  Button cancelButton;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_register);

    saveIndustryButton = (Button) findViewById(R.id.saveIndustryButton);
    cancelButton = (Button) findViewById(R.id.cancelButton);

    // Find the edit text
    industriaNombreE = (EditText) findViewById(R.id.industriaNombreE);
    representanteE = (EditText) findViewById(R.id.representanteE);
    emailE = (EditText) findViewById(R.id.emailE);
    areaIndustriaE = (EditText) findViewById(R.id.areaIndustriaE);

    saveIndustryButton.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        saveRegister();
      }
    });

    cancelButton.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        showHome();
      }
    });
  }

  private void saveRegister() {
    DAOIndustry dao = new DAOIndustry();

    // Values
    String industriaNombre = industriaNombreE.getText().toString();
    String representante = representanteE.getText().toString();
    String email = emailE.getText().toString();
    String areaIndustria = areaIndustriaE.getText().toString();

    boolean canSave = !industriaNombre.equals("") && !representante.equals("") && !email.equals("") && !areaIndustria.equals("");

    if (canSave) {
      Industry industry = new Industry(industriaNombre, representante, email, areaIndustria);
      // Prepare Object Industry
      dao.add(industry).addOnSuccessListener(new OnSuccessListener<Void>() {
        @Override
        public void onSuccess(Void unused) {
          showAlert("Agregado", "Industria agregada");
          clearForm();
        }
      }).addOnFailureListener(new OnFailureListener() {
        @Override
        public void onFailure(@NonNull Exception e) {
          showAlert("Error", "Error al agregar");
        }
      });
    } else {
      showAlert("Advertencia", "Llenar todos los datos");
    }
  }

  private void showHome() {
    Intent homeIntent = new Intent(this, HomeActivity.class);
    startActivity(homeIntent);
    finish();
  }

  private void showAlert(String title, String msg) {
    AlertDialog.Builder builder = new AlertDialog.Builder(this);
    builder.setTitle(title);
    builder.setMessage(msg);
    builder.setPositiveButton("Aceptar", null);
    AlertDialog dialog = builder.create();
    dialog.show();
  }

  private void clearForm() {
    industriaNombreE.setText("");
    representanteE.setText("");
    emailE.setText("");
    areaIndustriaE.setText("");
  }
}