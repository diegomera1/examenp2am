package com.example.examenam2;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class RVAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
  private Context context;
  ArrayList<Industry> list = new ArrayList<>();
  private ItemClickListener itemClickListener;

  public RVAdapter(Context ctx, ItemClickListener itemClickListener) {
    this.context = ctx;
    this.itemClickListener = itemClickListener;
  }
  public void setItems(ArrayList<Industry> industries) {
    list.addAll(industries);
  }
  @NonNull

  @Override
  public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
    View view = LayoutInflater.from(context).inflate(R.layout.layout_item, parent, false);
    return new IndustryVH(view);
  }

  @Override
  public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
    IndustryVH vh = (IndustryVH) holder;
    Industry industry = list.get(position);
    vh.industriaNombreTextView.setText(industry.getIndustriaNombre());
    vh.repesentanteTextView.setText(industry.getRepresentante());

    holder.itemView.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        itemClickListener.onItemClick(industry);
      }
    });
  }
  @Override
  public int getItemCount() {
    return list.size();
  }

  public interface ItemClickListener {
    void onItemClick(Industry industry);
  }
}