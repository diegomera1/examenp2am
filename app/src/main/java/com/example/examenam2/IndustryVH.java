package com.example.examenam2;

import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class IndustryVH extends RecyclerView.ViewHolder {
  public TextView industriaNombreTextView, repesentanteTextView;

  public IndustryVH (@NonNull View itemView) {
    super (itemView);

    industriaNombreTextView = (TextView) itemView.findViewById(R.id.industriaNombreTextView);
    repesentanteTextView = (TextView) itemView.findViewById(R.id.repesentanteTextView);
  }
}
