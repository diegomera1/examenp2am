package com.example.examenam2;

import android.os.Bundle;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;

public class DetailActivity extends AppCompatActivity {
  String key;
  DAOIndustry dao;
  Industry industry;
  TextView industriaNombreTextView, representanteTextView, emailTextView, areaIndustriaTextView;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_detail);

    industriaNombreTextView = (TextView) findViewById(R.id.industriaNombreTextView);
    representanteTextView = (TextView) findViewById(R.id.representanteTextView);
    emailTextView = (TextView) findViewById(R.id.emailTextView);
    areaIndustriaTextView = (TextView) findViewById(R.id.areaIndustriaTextView);

    key = getIntent().getStringExtra("object_key");
    dao = new DAOIndustry();

    dao.getSingle(key).addValueEventListener(new ValueEventListener() {
      @Override
      public void onDataChange(@NonNull DataSnapshot snapshot) {
        for (DataSnapshot data: snapshot.getChildren()) {
          industry = data.getValue(Industry.class);
        }
        showData();
      }

      @Override
      public void onCancelled(@NonNull DatabaseError error) {
      }
    });
  }

  private void showData() {
    // Set Tile
    setTitle("Industria: " + industry.getIndustriaNombre());

    // Set details
    industriaNombreTextView.setText(industry.getIndustriaNombre());
    representanteTextView.setText(industry.getRepresentante());
    emailTextView.setText(industry.getEmail());
    areaIndustriaTextView.setText(industry.getAreaIndustria());
  }
}