package com.example.examenam2;

import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;

public class DAOIndustry {
  private DatabaseReference databaseReference;

  public DAOIndustry() {
    FirebaseDatabase db = FirebaseDatabase.getInstance();
    databaseReference = db.getReference(Industry.class.getSimpleName());
  }

  public Task<Void> add(Industry industry) {
    return databaseReference.push().setValue(industry);
  }

  public Query get(String key) {
    if (key == null) {
      return databaseReference.orderByKey().limitToFirst(8);
    }

    return databaseReference.orderByKey().startAfter(key).limitToFirst(8);
  }

  public Query getSingle(String key) {
    return databaseReference.orderByKey().startAt(key).limitToFirst(1);
  }
}
