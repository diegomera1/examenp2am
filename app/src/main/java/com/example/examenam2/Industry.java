package com.example.examenam2;

import com.google.firebase.database.Exclude;

public class Industry {
  @Exclude
  private String key;
  private String industriaNombre;
  private String representante;
  private String email;
  private String areaIndustria;

  public Industry () {}

  public Industry(String industriaNombre, String representante, String email, String areaIndustria) {
    this.industriaNombre = industriaNombre;
    this.representante = representante;
    this.email = email;
    this.areaIndustria = areaIndustria;
  }

  public String getIndustriaNombre() {
    return industriaNombre;
  }

  public void setIndustriaNombre(String industriaNombre) { this.industriaNombre = industriaNombre;
  }

  public String getRepresentante() {
    return representante;
  }

  public void setRepresentante(String representante) {
    this.representante = representante;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getAreaIndustria() {
    return areaIndustria;
  }

  public void setAreaIndustria(String areaIndustria) {
    this.areaIndustria = areaIndustria;
  }

  public String getKey() {
    return key;
  }

  public void setKey(String key) {
    this.key = key;
  }
}